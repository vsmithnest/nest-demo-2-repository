package com.nest.demo;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import com.nest.demo.external.*;

import java.util.logging.Logger;

public class DelegateAlgorithms implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        // Get mortgage
        String mortgage = (String) execution.getVariable("mortgage");

        // Get Random Validated Boolean
        Boolean Validated = (Boolean) ExecuteJava.getRandomBoolean(70f);
        // Set Validated
        execution.setVariable("Validated", Validated);

        LOGGER.info("Processing mortgage '" + mortgage + "'whose status is: '" + Validated + "'...");
    }

}