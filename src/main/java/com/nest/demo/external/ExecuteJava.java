package com.nest.demo.external;

public class ExecuteJava {
    static public boolean getRandomBoolean(float probability) {
        double randomValue = Math.random()*100;  //0.0 to 99.9
        return randomValue <= probability;
    }

    public static void main(String[] args) {

        // Test the results of the isValid method.
        System.out.println(getRandomBoolean(90f));
        System.out.println(getRandomBoolean(20f));
        System.out.println(getRandomBoolean(50f));
    }
}
